const User = require('../models/users');
const bcrypt = require('bcryptjs');
const auth = require('../auth');


exports.login_handler = async (req,res) =>{
    try{
        if (!req.body.username || !req.body.password) {
            return res.json({ success: false, msg: 'Invalid credentials' }).status(400);
        }
        else{
            const user = await User.findOne({username:req.body.username});
            if(!user){
                return res.json({ success: false, msg: 'Invalid credentials' }).status(400);
            }
            else{
                const result = await bcrypt.compare(req.body.password,user.password);
                if(result){
                    return res.json({ success: true, msg:{convey:'Logged in successfully',token:auth.issueToken(user)}  }).status(200);
                }
                else{
                    return res.json({ success: false, msg: 'Invalid credentials' }).status(400);
                }
            }
        }
    }
    catch(err){
        return res.json({success:false,msg:err}).status(500);
    }
}




exports.password_change_handler = async (req, res) =>{
    // try{
        if(!req.user.username || !req.body.old_password || !req.body.new_password){
            return res.json({ success: false, msg: 'No user detected' }).status(400);
        }
        else{
            const user = await User.findOne({ username: req.user.username });
            const check = await bcrypt.compare(req.body.old_password,user.password)
            if(user && check){
                const salt = await bcrypt.genSalt(10);
                const password = await bcrypt.hash(req.body.new_password,salt);
                const result = await User.findOneAndUpdate({username:req.user.username},{password:password});
                if(result) return res.json({success: true, msg: 'Password Updated' }).status(200);
            }   
        }
        return res.json({ success: false, msg: 'No user detected' }).status(400);
    // }
    // catch(err){
    //     res.json({success:false,msg:err}).status(500);
    // }
}

