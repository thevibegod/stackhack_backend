const Event = require('../models/events');
const formidable = require('formidable');
const fs = require('fs');

// creates an event
exports.create_handler = (req, res) => {

    let form = new formidable.IncomingForm();
    form.parse(req, (err, fields, files) => {
        const eventImg = files.eventImg;
        fs.readFile(eventImg.path, 'base64', (err, data) => {
            if (err) throw err;
            if (data) {
                let accepting_from, accepting_till;
                if (!fields.accepting_from) {
                    accepting_from = new Date();
                }
                else {
                    accepting_from = fields.accepting_from
                }
                if (!fields.accepting_till) {
                    accepting_till = fields.date;
                }
                else {
                    accepting_till = fields.accepting_till
                }
                // no need for special validation since data is validated in the frontend
                const event = new Event({
                    name: fields.name,
                    category: fields.category,
                    venue: fields.venue,
                    coords: { lat: fields.lat, lng: fields.lng },
                    date: fields.date,
                    accepting_from: accepting_from,
                    accepting_till: accepting_till,
                    max_participants: fields.max_participants,
                    participant_count: 0,
                    image: data.toString('basee64')
                });
                event.save()
                    .then(data => {
                        return res.json({ success: true, msg: 'Event created', id: event.id }).status(201)
                    })
                    .catch(err => {
                        return res.json({ success: false, msg: err }).status(500)
                    })
            }
        })

    })

}

// edits an event
exports.edit_handler = (req, res) => {
    if (!req.body.id) {
        return res.json({ success: false, msg: 'No event id' }).status(400)
    }
    // no need for special validation since data is validated in the frontend
    Event.findOneAndUpdate({ _id: req.body.id }, {
        name: req.body.name,
        category: req.body.category,
        venue: req.body.venue,
        coords: { lat: req.body.lat, lng: req.body.lng },
        date: req.body.date,
        accepting_from: req.body.accepting_from,
        accepting_till: req.body.accepting_till,
        max_participants: req.body.max_participants
    }, { new: true })
        .then(data => {
            return res.json({ success: true, msg: 'Event Edited' }).status(200)
        })
        .catch(err => {
            return res.json({ success: false, msg: err }).status(500)
        })
}

// deletes an event
exports.delete_handler = (req, res) => {
    if (!req.body.id) {
        return res.json({ success: false, msg: 'No event id' })
    }
    // no need for special validation since data is validated in the frontend
    Event.findOneAndDelete({ _id: req.body.id })
        .then(data => {
            return res.json({ success: true, msg: 'Event Deleted' }).status(200)
        })
        .catch(err => {
            return res.json({ success: false, msg: err }).status(500)
        })
}

// gets list all events
exports.list_handler = (req, res) => {
    Event.find({}).then(events => {
        return res.json({ success: true, events: events }).status(200)
    })
}

// gets a particular event
exports.get_single_event_handler = (req, res) => {
    Event.findOne({ _id: req.query.id }).then(event => {
        return res.json({ success: true, event: event }).status(200)
    })
}