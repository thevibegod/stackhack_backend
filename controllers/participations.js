const Participation = require("../models/participations")
const Event = require("../models/events")
const formidable = require("formidable")
const fs = require('fs')
const custom_id = require('custom-id')


// gets participations for an event.
exports.for_event_handler = async (req, res) => {
    try {
        const event_id = req.params.event_id
        const event = await Event.findById(event_id);
        if (!event) {
            return res.json({
                success: false,
                msg: "no Event with such ID"
            })
        }
        const participations = await Participation.find({ event_id: event_id })
        if (!participations) {
            return res.json({
                success: false,
                msg: 'Some error occured'
            })
        } else {
            return res.json({
                success: true,
                msg: {
                    event,
                    participations
                }
            }).status(200)
        }
    } catch (err) {
        return res.json({
            success: false,
            msg: 'Server Error'
        }).status(500)
    }
}


// Unique Participation gets returned along with
exports.unique_handler = async (req, res) => {
    try {
        const id = req.params.id
        const participation = await Participation.findById(id)

        if (!participation) {
            return res.json({
                success: false,
                msg: 'No Participation with such ID'
            }).status(400)
        }

        const for_event = await Event.findById(participation.event_id)

        if (!for_event) {
            return res.json({
                success: false,
                msg: 'Suspicious'
            }).status(403)
        }

        return res.json({
            success: true,
            msg: { participation, for_event }
        }).status(200)
    } catch (err) {
        return res.json({
            success: false,
            msg: 'Server Error'
        }).status(500)
    }
}


// creates a participation for an event
exports.create_handler = (req, res) => {
    try {
        const event_id = req.params.event_id
        const form = new formidable.IncomingForm()
        form.parse(req, async (err, fields, files) => {
            if (fields.ticket_count < 1) {
                return res.json({
                    success: false,
                    msg: 'Ticket count must atleast be 1.'
                }).status(400)
            }

            if (!files.id_card) {
                return res.json({
                    success: false,
                    msg: 'No ID card uploaded'
                }).status(400)
            }

            const event = await Event.findById(event_id);

            if (!event) {
                res.json({
                    success: false,
                    msg: 'no such event'
                }).status(400)
            }

            if (event.max_participants - event.participant_count < fields.ticket_count) {
                return res.json({
                    success: false,
                    msg: 'Registration sold out for the ticket count provided'
                }).status(400)
            }

            var { firstName, lastName, email, registration_type, mobile, ticket_count, ...rest } = fields
            ticket_count = parseInt(ticket_count)
            var participation_data = {}
            if (fields.registration_type == "OTHER") {
                if (!fields.other_type) {
                    return res.json({
                        success: false,
                        msg: 'Registration type mentioned is incorrect'
                    }).status(400)
                }
                participation_data = {
                    firstName,
                    lastName,
                    email,
                    registration_type,
                    ticket_count,
                    mobile,
                    other_type: fields.other_type
                }
            }
            else {
                participation_data = { firstName, lastName, email, registration_type, ticket_count, mobile }
            }
            console.log(registration_type + " --> reg type");
            const id_img = files.id_card
            let image_string = ""
            fs.readFile(id_img.path, 'base64', async (err, data) => {
                if (err) throw err
                image_string = data.toString('base64')
                // console.log(image_string)
                participation_data.id_card = image_string
                participation_data.unique_id = custom_id({
                    event_id: event_id,
                    number: mobile,
                    firstName: firstName,
                    lastName: lastName,
                    email: email
                })
                const participation = new Participation({ event_id: event_id, ...participation_data })
                // console.log(participation.id_card)
                event.participant_count += ticket_count
                const saved_event = await event.save()
                console.log('im here')
                const saved_participation = await participation.save();
                if (!saved_participation) {
                    res.json({
                        success: false,
                        msg: 'Error saving participation'
                    }).status(400)
                }

                return res.json({
                    success: true,
                    msg: {
                        participation,
                        saved_event,
                        convey: 'Participation saved successfully.'
                    }
                }).status(200)

            })



        })
    } catch (err) {

    }
}
