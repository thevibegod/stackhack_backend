const jwt = require('jsonwebtoken');
const secret = 'djckdsjcldskseqjsdve53svc213ewddref4x5rrf1xr42f44rfx4gfx5t435fc4t43fXRxr34s21fcd14x15zwef';

// issues a jwt token
exports.issueToken = (user) => {
    return jwt.sign({username:user.username,password:user.password}, secret,{expiresIn:"1h"})
}

// middleware for verifying incoming jwt tokens
exports.authMiddleware = (req, res, next) => {
    token = req.headers.authorization;
    if(token){
        jwt.verify(token,secret,(err,user)=>{
            if(err){
                return res.json({'success':false,'message':'Invalid token'}).status(403)
            }
            req.user = user;
            next();
        })
    }
    else{
        return res.json({'success':false,'message':'No token'}).status(400)
    }

}

