const bcrypt = require('bcryptjs');
const hashPassword = (password) => {
    const hash = bcrypt.hash(password, 10, (err, hash) => {
        if (err) {
            throw err;
        } else {
            return hash;
        }
    });
    return hash
}

console.log(hashPassword('asmin'))