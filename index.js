const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const {issueToken, authMiddleware} = require('./auth');
const dbConfig = require('./config');
const mongoose = require('mongoose');
const events = require('./routes/events');
const users = require('./routes/users');
const participations = require('./routes/participations')
mongoose.Promise = global.Promise;

// connecting to database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});
// const user = require('./models/users')
// new user({username:'admin',password:'admin'}).save().then(res=>console.log(res))

// to parse form data from frontend
app.use(bodyParser.urlencoded({extended: false, limit: '10mb'}));
app.use(bodyParser.json({limit: '10mb'}));


app.get('/', (req, res) => {
    res.json({'success': true, 'token': issueToken('admin')}).status(200)
})
app.use('/api/events', events);
app.use('/api/participations',participations)
app.use('/api/users', users);

app.listen(8000, () => {
    console.log('Backend service running');
});