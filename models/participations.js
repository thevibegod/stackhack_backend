const mongoose = require('mongoose');

const participationSchema = mongoose.Schema({
    event_id:{
        type:mongoose.Types.ObjectId,
        ref:'Event'
    },
    unique_id:{
        type:String,
        maxlength:10
    },
    firstName: {
        type: String,
        require: true
    },
    lastName: {
        type: String
    },
    mobile:{
        type:String
    },
    email: {
        type: String,
        lowercase: true,
        trim: true
    },
    id_card: {
        type: String,
        require: true,
    },
    registration_type: {
        type: String,
        enum: ['SELF', 'GROUP', 'CORPORATE', 'OTHERS']
    },
    other_type: {
        type: String,
        default: ""
    },
    ticket_count: {
        type: Number,
        validate: {
            validator: Number.isInteger,
            message: 'Ticket count must be an Integer'
        }
    },
    registration_failed:{
        type:Boolean,
        default:false
    }
});

module.exports = new mongoose.model('Participation', participationSchema);