const mongoose = require('mongoose');

const eventSchema = mongoose.Schema({
    name: { type: String, require: true },
    category: { type: String, require: true },
    venue: { type: String, require: true },
    coords: { lat: String, lng: String },
    date: { type: Date, require: true },
    accepting_from: { type: Date },
    accepting_till: { type: Date },
    max_participants: { type: Number, require: true },
    participant_count: { type: Number },
    render: { type: Boolean, default: true },
    image : String

});

module.exports = new mongoose.model('Event', eventSchema);