const events_controller = require('../controllers/events');
const router = require('express').Router();
const auth = require('../auth');

// all event routes

router.get('/all',events_controller.list_handler);
router.get('/event',events_controller.get_single_event_handler);


router.use(auth.authMiddleware);

router.post('/create',events_controller.create_handler);
router.post('/edit',events_controller.edit_handler);
router.delete('/delete',events_controller.delete_handler);

module.exports = router;
