const user_controller = require('../controllers/users')
const router = require('express').Router()
const auth = require('../auth');

router.post('/login',user_controller.login_handler);

router.use(auth.authMiddleware);

router.post('/changepassword',user_controller.password_change_handler);

module.exports = router;