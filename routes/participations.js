const router = require("express").Router()
const participation_controllers = require('../controllers/participations')


// all paticipation routes

router.get('/forEvent/:event_id',participation_controllers.for_event_handler)
router.get('/unique/:id', participation_controllers.unique_handler)
router.post('/:event_id/create_participation', participation_controllers.create_handler)


module.exports = router;